import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.testing.gateway.YandexTranslationGateway;
import ru.testing.pojo.Translations;

import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

public class YandexTranslationTest {
    private static final String FOLDER_ID = "b1g4sitm432fo13dkji3";
    private static final List<String> TEXTS = Arrays.asList("Hello","World!");
    private static final String TARGET_LANGUAGE_CODE = "ru";

    @lombok.SneakyThrows
    @Test
    @DisplayName("������� Hello world")
    public void getMoscowWeather(){
        try {
            YandexTranslationGateway yandexTranslationGateway = new YandexTranslationGateway();
            Translations translations = yandexTranslationGateway.getTranslations(FOLDER_ID, TEXTS, TARGET_LANGUAGE_CODE);
            Assertions.assertEquals(translations.getTranslations().get(0).getText(),"������");
            Assertions.assertEquals(translations.getTranslations().get(1).getText(), "���!");
        }
        catch (Exception e){

        }
    }

}
