package ru.testing.pojo;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Request {
    @SerializedName("text")
    private String text;
    @SerializedName("detectedLanguageCode")
    private String detectedLanguageCode;

    public String getText() {
        return text;
    }
}
