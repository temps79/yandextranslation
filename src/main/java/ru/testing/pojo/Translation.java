package ru.testing.pojo;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

import java.util.ArrayList;

@Getter
@Builder
public class Translation {
    @SerializedName("folderId")
    public String folderId;
    @SerializedName("texts")
    public ArrayList<String> texts;
    @SerializedName("targetLanguageCode")
    public String targetLanguageCode;



}
