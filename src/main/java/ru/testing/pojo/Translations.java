package ru.testing.pojo;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class Translations {
    @SerializedName("translations")
    List<Request> translations;

    public List<Request> getTranslations() {
        return translations;
    }
}
