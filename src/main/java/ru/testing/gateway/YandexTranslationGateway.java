package ru.testing.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import ru.testing.pojo.Translations;

import java.util.List;


public class YandexTranslationGateway {
    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String TOKEN = "AQVNw9AM5AZZYb41fBcNO2HYFDNH0ognMz_ak5zZ ";


    @lombok.SneakyThrows
    public Translations getTranslations(String folderId, List<String> texts, String targetLanguageCode) throws UnirestException{
        Gson gson = new Gson();
        HttpResponse<String> response = Unirest.post(URL)
                .header("Accept", "application/json")
                .header("Authorization", String.format("Api-Key %s", TOKEN))
                .queryString("folderId", folderId)
                .queryString("texts", texts)
                .queryString("targetLanguageCode", targetLanguageCode)
                .asString();
        String strResponse = response.getBody();
        return gson.fromJson(strResponse, Translations.class);
    }
}
